
<!-- html -->
<li class="nav-item active">
    <a class="nav-link" href="admin.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>หน้าหลัก</span></a>
</li>


<li class="nav-item">
    <a class="nav-link" href="admin_plant_table.php">
        <i class="fas fa-fw fa-table"></i>
        <span>ตารางต้นไม้ทั้งหมด</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin_detail_table.php">
        <i class="fas fa-fw fa-table"></i>
        <span>ตารางพรรณไม้ทั้งหมด</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin_detail_table.php">
        <i class="fas fa-fw fa-table"></i>
        <span>ตาราง Season</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin_detail_table.php">
        <i class="fas fa-fw fa-table"></i>
        <span>ตาราง family</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="admin_Insert_plant.php">
        <i class="fas fa-fw fa-table"></i>
        <span>เพิ่มต้นไม้ใหม่</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin_Insert_detail.php">
        <i class="fas fa-fw fa-table"></i>
        <span>เพิ่มพรรณไม้ใหม่</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin_insert_season.php">
        <i class="fas fa-fw fa-table"></i>
        <span>เพิ่ม Season ใหม่</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin_insert_family.php">
        <i class="fas fa-fw fa-table"></i>
        <span>เพิ่ม Familiy ใหม่</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="admin_imguploads.php">
        <i class="fas fa-fw fa-table"></i>
        <span>เพิ่มรูปภาพ</span></a>
</li>
