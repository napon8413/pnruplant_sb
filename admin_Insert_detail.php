<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">

<?php
include('material_head.php');
?>

<body id="page-top">
    <?php
    if ($_SESSION["user_login"]) {
    ?>
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-tree"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">PNRU PLANT ADMIN </div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Nav Item - Dashboard -->
                <?php
            include('material_Nav_Item_Dashboard.php');
            ?>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">
            </ul>
            <!-- End of Sidebar -->



            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->


                        <!-- Topbar Navbar -->
                        <?php
                        include('material_admin_topbar.php');
                        ?>

                    </nav>
                    <!-- End of Topbar -->



                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">เพิ่มข้อมูลพรรณไม้ใหม่</h1>
                        <div class="container">
                            <form action="admin_insert_sql_detail.php" method="post">
                                <div class="form-row">

                                    <!-- PlantID -->
                                    <?php
                                    //1. เชื่อมต่อ database: 
                                    include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

                                    //2. query ข้อมูลจากตาราง tb_member: 
                                    $query = "SELECT PlandetailtID FROM plantdetail ORDER BY PlandetailtID DESC LIMIT 1" or die("Error:" . mysqli_error());
                                    //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                    $results = mysqli_query($conn, $query);

                                    //4 . แสดงข้อมูลที่ query ออกมา โดยใช้ตารางในการจัดข้อมูล: 
                                    while ($row = mysqli_fetch_assoc($results)) {
                                        $PlandetailtID = substr($row["PlandetailtID"], -3);
                                        $PlandetailtID = intval($PlandetailtID) + 1;
                                        $PlandetailtID = "64201-10220-99-" . str_pad($PlandetailtID, 3, '0', STR_PAD_LEFT);
                                        echo "<div class='form-group col-md-6'>
                            <label for='PlandetailtID'>PlandetailtID</label>
                            <input type='text' class='form-control' name='PlandetailtID' value='" . $PlandetailtID . "'>
                            </div>";
                                    }

                                    ?>


                                    <!-- PlantName -->
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">ชื่อพรรณไม้ PlantName</label>
                                        <input type="text" class="form-control" name="PlantName">
                                    </div>
                                    <!-- PlantScience -->
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">ชื่อวิทยาศาสตร์ PlantScience</label>
                                        <input type="text" class="form-control" name="PlantScience">
                                    </div>
                                    <!-- PlantDiscover -->
                                    <div class="form-group col-md-6">
                                        <label for="PlantDiscover">ชื่อผู้ค้นพบ PlantDiscover</label>
                                        <input type="text" class="form-control" name="PlantDiscover">
                                    </div>
                                </div>
                                <!-- PlantCommonname -->
                                <div class="form-group">
                                    <label for="PlantCommonname">ชื่อสามัญ PlantCommonname</label>
                                    <input type="text" class="form-control" name="PlantCommonname" placeholder="">
                                </div>
                                <!-- PlantType -->
                                <div class="form-group">
                                    <label for="PlantType">ประเภท</label>
                                    <input type="text" class="form-control" name="PlantType" placeholder="ประเภท เช่น ไม้ยืนต้น">
                                </div>
                                <!-- PlantTypeENG -->
                                <div class="form-group">
                                    <label for="PlantTypeENG">PlantType</label>
                                    <input type="text" class="form-control" name="PlantTypeEng" placeholder="ประเภท เช่น ไม้ยืนต้น">
                                </div>
                                <!-- PlantDistrbution -->
                                <div class="form-group">
                                    <label for="PlantDistrbution">สถานที่ค้นพบ</label>
                                    <textarea class="form-control" name="PlantDistrbution" rows="4"></textarea>
                                </div>
                                <!-- PlantDistrbutionEng -->
                                <div class="form-group">
                                    <label for="PlantDistrbutionEng">PlantDistrbution</label>
                                    <textarea class="form-control" name="PlantDistrbutionEng" rows="4"></textarea>
                                </div>
                                <!-- PlantBenefit -->
                                <div class="form-group">
                                    <label for="PlantBenefit">ประโยชน์</label>
                                    <textarea class="form-control" name="PlantBenefit" rows="4"></textarea>
                                </div>
                                <!-- PlantBenefitEng -->
                                <div class="form-group">
                                    <label for="PlantBenefitEng">PlantBenefit</label>
                                    <textarea class="form-control" name="PlantBenefitEng" rows="4"></textarea>
                                </div>
                                <!-- PlantBanefity -->
                                <div class="form-group">
                                    <label for="PlantBanefity">ประโยชน์อื่นๆ</label>
                                    <textarea class="form-control" name="PlantBanefity" rows="4"></textarea>
                                </div>
                                <!-- PlantBanefityEng -->
                                <div class="form-group">
                                    <label for="PlantBanefityEng">PlantBanefity</label>
                                    <textarea class="form-control" name="PlantBanefityEng" rows="4"></textarea>
                                </div>
                                <!-- PlantFlower -->
                                <div class="form-group">
                                    <label for="PlantFlower">รายละเอียดดอก</label>
                                    <textarea class="form-control" name="PlantFlower" rows="4"></textarea>
                                </div>
                                <!-- PlantFlowerEng -->
                                <div class="form-group">
                                    <label for="PlantFlowerEng">PlantFlower</label>
                                    <textarea class="form-control" name="PlantFlowerEng" rows="4"></textarea>
                                </div>
                                <!-- PlantRound -->
                                <div class="form-group">
                                    <label for="PlantRound">รายละเอียดผล</label>
                                    <textarea class="form-control" name="PlantRound" rows="4"></textarea>
                                </div>
                                <!-- PlantRoundEng -->
                                <div class="form-group">
                                    <label for="PlantRoundEng">PlantRound</label>
                                    <textarea class="form-control" name="PlantRoundEng" rows="4"></textarea>
                                </div>
                                <!-- PlantSeed -->
                                <div class="form-group">
                                    <label for="PlantSeed">รายละเอียดเมล็ด</label>
                                    <textarea class="form-control" name="PlantSeed" rows="4"></textarea>
                                </div>
                                <!-- PlantSeedEng -->
                                <div class="form-group">
                                    <label for="PlantSeedEng">PlantSeed</label>
                                    <textarea class="form-control" name="PlantSeedEng" rows="4"></textarea>
                                </div>
                                <!-- PlantStem -->
                                <div class="form-group">
                                    <label for="PlantStem">รายละเอียดลำต้น</label>
                                    <textarea class="form-control" name="PlantStem" rows="4"></textarea>
                                </div>
                                <!-- PlantStemEng -->
                                <div class="form-group">
                                    <label for="PlantStemEng">PlantStem</label>
                                    <textarea class="form-control" name="PlantStemEng" rows="4"></textarea>
                                </div>
                                <!-- PlantLeaf -->
                                <div class="form-group">
                                    <label for="PlantLeaf">รายละเอียดใบ</label>
                                    <textarea class="form-control" name="PlantLeaf" rows="4"></textarea>
                                </div>
                                <!-- PlantLeafEng -->
                                <div class="form-group">
                                    <label for="PlantLeafEng">PlantLeaf</label>
                                    <textarea class="form-control" name="PlantLeafEng" rows="4"></textarea>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputSeasonID">ฤดู</label>
                                        <select name="inputSeasonID" class="form-control">
                                            <?php
                                            include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

                                            //2. query ข้อมูลจากตาราง tb_member: 
                                            $queryseason = "SELECT * FROM `season`" or die("Error:" . mysqli_error());
                                            //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                            $resultsseason = mysqli_query($conn, $queryseason);

                                            //4 . แสดงข้อมูลที่ query ออกมา โดยใช้ตารางในการจัดข้อมูล: 
                                            while ($rowseason = mysqli_fetch_assoc($resultsseason)) {
                                                echo "<option value='" . $rowseason['SeasonID '] . "'>" . $rowseason['SeasonName'] . "</option>";
                                            }
                                            mysqli_close($conn);
                                            ?>
                                        </select>
                                    </div>

                                    <?php
                                    include('material_admin_loop_familyname.php');
                                    ?>
                                </div>

                                <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2020</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <?php
        include('material_Logout_Modal.php');
        ?>

        <!-- script -->
        <?php
        include('material_script.php');
        ?>
    <?php
    } else {
        echo "<h1>Please login first .</h1>";
        echo "<a class='btn btn-primary' href='admin_singin.php'>หน้าหลัก</a>";
    }
    ?>
</body>

</html>