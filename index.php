<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">

    <title>PNRUPLANT</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>

<body>

    <!-- !Section Navbar -->
    <nav id="navbar" class="navbar navbar-expand-lg position-sticky navbar-dark bg-alpha">
        <div class="container">
            <a class="navbar-brand" href="index.php">
                <img src="image/pnru_logo.png" width="35" height="35" class="d-inline-block align-top" alt="">
                PNRUPLANT
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarKey"
                aria-controls="navbarKey" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarKey">
                <ul class="navbar-nav ml-auto text-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="#search">หน้าหลัก <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">เกี่ยวกับเรา</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="#input">เพิ่มข้อมูล</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href="admin_singin.php">Admin login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- !Section Navbar -->


    <!-- !Section search ค้นหา -->
    <div class="jumbotron jumbotron-fluid text-center row" id="search">
        <div class="col"></div>
        <div class="container col">
            <h1 class="border-short-bottom">ระบบค้นหาข้อมูลพรรณไม้ PNRUPLANT</h1>
            <p class="lead">กรอกชื่อพรรณไม้</p>
            <div class="container">
                <form class="" action="user_search.php" method="POST">
                    <div class="">
                        <input name="plantname" type="text" class="form-control text-center" id="inputPassword2"
                            placeholder="ชื่อพรรณไม้">

                    </div>
                    <div class="container" style="padding: 20px;">
                        <button type="submit" class="btn btn-primary mb-2">ค้นหา</button>
                    </div>
                </form>
            </div>
            <div>
                <a href="user_showdata.php" class="dropdown-item ">ดูต้นไม้ทั้งหมด</a>
                <a href="user_showdetail.php" class=" dropdown-item ">ดูพรรณไม้ทั้งหมด</a>
            </div>
        </div>
        <div class="col"></div>
    </div>

    <?php
    include('material_aboutme.php');
    ?>

    <footer class="footer">
        <span> COPYRIGHT © 2020
            <a href="#" target="_blank">Soymilk</a>
            ALL Right Reserved
        </span>
    </footer>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    

</body>

</html>