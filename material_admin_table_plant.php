<div class="card shadow mb-4" id="allplant">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">ต้นไม้ทั้งหมด</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <?php

                                        //1. เชื่อมต่อ database: 
                                        include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี
                                        //2. query ข้อมูลจากตาราง tb_member: 
                                        $query3 = "SELECT area.*,plantdetail.* FROM area LEFT JOIN plantdetail ON area.PlandetailtID = plantdetail.PlandetailtID " or die("Error:" . mysqli_error());
                                        //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                        $result3 = mysqli_query($conn, $query3);
                                        ?>
                                        <tr>
                                            <th>รหัสต้นไม้</th>
                                            <th>ชื่อพรรณไม้</th>
                                            <th>รหัสพื้นที่</th>
                                            <th>รหัสพรรณไม้</th>
                                            <th>ลองจิจูด</th>
                                            <th>ละติจูด </th>
                                            <th>สถานะ</th>
                                            <th>วันที่เพิ่ม</th>
                                            <!-- <th>qrcode</th> -->
                                            <th>แก้ไข</th>
                                            <th>ลบ</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                        <th>รหัสต้นไม้</th>
                                        <th>ชื่อพรรณไม้</th>
                                            <th>รหัสพื้นที่</th>
                                            <th>รหัสพรรณไม้</th>
                                            <th>ลองจิจูด</th>
                                            <th>ละติจูด </th>
                                            <th>สถานะ</th>
                                            <th>วันที่เพิ่ม</th>
                                            <!-- <th>qrcode</th> -->
                                            <th>แก้ไข</th>
                                            <th>ลบ</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        while ($row3 = mysqli_fetch_array($result3)) {
                                            echo "<tr>";
                                            echo "<th><a href='admin_Plantdetail.php?ID=$row3[0]'>" . $row3["plantlocationID"] .  "</th> ";
                                            echo "<td>" . $row3["PlantName"] .  "</td> ";
                                            echo "<td>" . $row3["ZoneID"] .  "</td> ";
                                            echo "<td><a href='admin_detail.php?ID=$row3[2]'>" . $row3["PlandetailtID"] .  "</td> ";
                                            echo "<td>" . $row3["longtitudeY"] .  "</td> ";
                                            echo "<td>" . $row3["latitudeX"] .  "</td> ";
                                            echo "<td>" . $row3["statuss"] .  "</td> ";
                                            echo "<td>" . $row3["statusDate"] .  "</td> ";
                                            // echo "<td>" . $row3["qrcode"] .  "</td> ";
                                            //เมนูดูข้อมูลอัพเดท
                                            echo "<td><a href='admin_Update_Plantdetail.php?ID=$row3[0]'>แก้ไขข้อมูล</a><br></td>  ";
                                            //ลบข้อมูล
                                            echo "<td><a href='admin_DeleteArea.php?ID=$row3[0]' onclick=\"return confirm('คุณต้องการลบพรรณไม้ " . $row3["plantlocationID"] . " ใช่ไหม')\">ลบข้อมูล</a></td> ";
                                            echo "</tr>";
                                        }

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>