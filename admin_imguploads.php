<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">

<?php
include('material_head.php');
?>

<body id="page-top">
    <?php
    if ($_SESSION["user_login"]) {
    ?>
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-tree"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">PNRU PLANT ADMIN </div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <?php
                include('material_Nav_Item_Dashboard.php');
                ?>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">
            </ul>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->


                        <!-- Topbar Navbar -->
                        <?php
                        include('material_admin_topbar.php');
                        ?>

                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800">เพิ่มรูปภาพ</h1>
                            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
                        </div>

                        <form action="admin_upload.php" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="PlandetailtIDimg">เลือกพรรณไม้</label>
                                <select class="form-control" id="PlandetailtIDimg" name="PlandetailtIDimg">
                                    <?php
                                    //1. เชื่อมต่อ database: 
                                    include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี
                                    //2. query ข้อมูลจากตาราง tb_member: 
                                    $query = "SELECT plantlocationID FROM `area` ORDER BY `area`.`plantlocationID` ASC" or die("Error:" . mysqli_error());
                                    //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                    $results = mysqli_query($conn, $query);
                                    //4 . แสดงข้อมูลที่ query ออกมา โดยใช้ตารางในการจัดข้อมูล: 
                                    while ($row = mysqli_fetch_assoc($results)) {
                                        echo "<option value=" . $row["plantlocationID"] . ">" . $row["plantlocationID"] . "</option>";
                                    }
                                    mysqli_close($conn);
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="typeimg">ประเภทรูป:</label>

                                <select name="typeimg" id="typeimg">
                                    <option value="1">1-ภาพทั้งหมด</option>
                                    <option value="2">2-ภาพดอก</option>
                                    <option value="3">3-ภาพลําต้น</option>
                                    <option value="4">4-ภาพใบ</option>
                                    <option value="5">5-ภาพเมล็ด</option>
                                    <option value="6">6-ภาพผล</option>
                                </select>
                            </div>
                            Select Image File to Upload:
                            <p>file 1</p>
                            <input type="file" name="file">,<br>
                            <input type="submit" name="submit" value="Upload">
                        </form>


                    </div>
                    <!-- End of Main Content -->

                    <!-- Footer -->
                    <footer class="sticky-footer bg-white">
                        <div class="container my-auto">
                            <div class="copyright text-center my-auto">
                                <span>Copyright &copy; PNRU PLANT 2020</span>
                            </div>
                        </div>
                    </footer>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <?php
        include('material_Logout_Modal.php');
        ?>

        <!-- script -->
        <?php
        include('material_script.php');
        ?>
    <?php
    } else {
        echo "<h1>Please login first .</h1>";
        echo "<a class='btn btn-primary' href='admin_singin.php'>หน้าหลัก</a>";
    }
    ?>
</body>

</html>