<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<?php
include('material_head.php');
?>

<body id="page-top">
<?php
if($_SESSION["user_login"]) {
?>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-tree"></i>
                </div>
                <div class="sidebar-brand-text mx-3">PNRU PLANT ADMIN </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            <?php
            include('material_Nav_Item_Dashboard.php');
            ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->


                    <!-- Topbar Navbar -->
                    <?php
                    include('material_admin_topbar.php');
                    ?>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">หน้าหลัก</h1>
                        <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-6 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                ต้นไม้ทั้งหมด</div>
                                            <?php
                                            //1. เชื่อมต่อ database: 
                                            include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี
                                            //2. query ข้อมูลจากตาราง tb_member: 
                                            $query = "SELECT COUNT(area.plantlocationID) AS plantlocationID FROM area" or die("Error:" . mysqli_error());
                                            //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                            $results = mysqli_query($conn, $query);
                                           
                                            //4 . แสดงข้อมูลที่ query ออกมา โดยใช้ตารางในการจัดข้อมูล: 
                                            while ($row = mysqli_fetch_assoc($results)) {
                                                echo "<div class='h5 mb-0 font-weight-bold text-gray-800'>" . $row["plantlocationID"] . "</div>";
                                            }
                                            
                                            mysqli_close($conn);
                                            ?>

                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-tree fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-6 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">พรรณไม้ทั้งหมด</div>

                                            <?php
                                            //1. เชื่อมต่อ database: 
                                            include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี
                                            //2. query ข้อมูลจากตาราง tb_member: 
                                            $query = "SELECT COUNT(plantdetail.PlandetailtID) AS PlandetailtID FROM plantdetail" or die("Error:" . mysqli_error());
                                            //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                            $results = mysqli_query($conn, $query);
                                            //4 . แสดงข้อมูลที่ query ออกมา โดยใช้ตารางในการจัดข้อมูล: 
                                            while ($row = mysqli_fetch_assoc($results)) {
                                                echo "<div class='h5 mb-0 font-weight-bold text-gray-800'>" . $row["PlandetailtID"] . "</div>";
                                            }
                                            mysqli_close($conn);
                                            ?>



                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-tree fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Content Row -->

                        <!-- Color System -->

                        <div class="col-lg-6 mb-4">
                            <div class="card bg-light text-white shadow">
                                <div class="card-body">
                                    <a class="nav-link" href="admin_Insert_plant.php">เพิ่มต้นไม้ใหม่</a>
                                    <!-- <div class="text-white-50 small">#4e73df</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-4">
                            <div class="card bg-light text-white shadow">
                                <div class="card-body">
                                <a class="nav-link" href="admin_Insert_detail.php">เพิ่มพรรณไม้ใหม่</a>
                                    
                                    <!-- <div class="text-white-50 small">#1cc88a</div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-4">
                            <div class="card bg-light text-white shadow">
                                <div class="card-body">
                                    <a class="nav-link" href="admin.php#allplant">ดูต้นไม้ทั้งหมด</a>
                                    <!-- <div class="text-white-50 small"></div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mb-4">
                            <div class="card bg-light text-white shadow">
                                <div class="card-body">
                                    <a class="nav-link" href="admin_detail_table.php">ดูพรรณไม้ทั้งหมด</a>

                                    <!-- <div class="text-white-50 small">#f6c23e</div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                    <!-- DataTales Example 1-->
                    <?php
                        include('material_admin_table_plant.php');
                    ?>

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; PNRU PLANT 2020</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <?php
include('material_Logout_Modal.php');
?>
<!-- script -->
<?php
include('material_script.php');
?>
    <?php
}else {
    echo "<h1>Please login first .</h1>";
echo "<a class='btn btn-primary' href='admin_singin.php'>หน้าหลัก</a>";
}
?>
</body>

</html>