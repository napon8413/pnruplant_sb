<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">

<?php
include('material_head.php');
?>


<body id="page-top">
    <?php
    if ($_SESSION["user_login"]) {
    ?>
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-tree"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">PNRU PLANT ADMIN </div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <?php
                include('material_Nav_Item_Dashboard.php');
                ?>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">
            </ul>
            <!-- End of Sidebar -->



            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->


                        <!-- Topbar Navbar -->
                        <?php
                    include('material_admin_topbar.php');
                    ?>

                    </nav>
                    <!-- End of Topbar -->



                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">เพิ่มข้อมูลพรรณไม้ใหม่</h1>
                        <div class="container">
                            <form action="admin_Update_sql_Plantdetail.php" method="post">
                                <div class="form-row">

                                    <?php
                                    //รับ parameter มาเก็บในตัวแปร ID
                                    $ID = $_GET["ID"];
                                    //1. เชื่อมต่อ database: 
                                    include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

                                    // สิ่งที่ต้องการคือ 63201-10220-99-xxx เราไม่ต้องการ /001 ข้างหลัง
                                    // 2.1 substr เพื่อตัดรหัส 63201-10220-99-xxx ออก และเก็บในตัวแปร subID
                                    $subID = substr($ID, 18);
                                    // ใช้ trim ตัด /001 ที่เก็บในตัวแปร subID เราก็จะได้ 63201-10220-99-xxx เพื่อไป query ในตารางplantdetail
                                    $trimID = trim($ID, $subID);
                                    //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                    //2. query ข้อมูลจากตาราง plant: 
                                    $query = "SELECT * FROM area WHERE area.plantlocationID = '" . $ID . "' ";
                                    $result = mysqli_query($conn, $query);

                                    // จะแก้โดยการ เอาตัวแปร id แสดงรหัสและตักคำไป query มาจากฐานข้อมูล

                                    while ($row = mysqli_fetch_array($result)) {
                                        // echo "<div><p>PlantID : " . $ID . "</p></div>";
                                        // echo "<div><p>PlantName: " . $row['PlantName'] . "</p></div>";
                                        // echo "<div><p>PlantScience : <i>" . $row['PlantCommonname'] . "</i> " . $row['PlantDiscover'] . "</p></div>";
                                        // echo "<div><p>PlantDiscover : " . $row['PlantDiscover'] . "</p></div>";
                                        // echo "<div><p>PlantCommonname : " . $row['PlantCommonname'] . "</p></div>";
                                        // echo "<div><p>PlantType : " . $row['PlantType'] . "</p></div>";
                                        // echo "<div><p>PlantDistrbution : " . $row['PlantDistrbution'] . "</p></div>";
                                        // echo "<div><p>PlantBenefit : " . $row['PlantBenefit'] . "</p></div>";
                                        // echo "<div><p>PlantBanefity : " . $row['PlantBanefity'] . "</p></div>";
                                        // // echo "<div><p>PlantIcon : ".$row['PlantIcon']."</p></div>";
                                        // echo "<div><p>PlantFlower : " . $row['PlantFlower'] . "</p></div>";
                                        // echo "<div><p>PlantRound : " . $row['PlantRound'] . "</p></div>";
                                        // echo "<div><p>PlantSeed : " . $row['PlantSeed'] . "</p></div>";
                                        // echo "<div><p>PlantStem : " . $row['PlantStem'] . "</p></div>";
                                        // echo "<div><p>PlantLeaf : " . $row['PlantLeaf'] . "</p></div>";
                                        // echo "<div><p>SeasonID : " . $row['SeasonID'] . "</p></div>";
                                        // echo "<div><p>PlantfamilyID :" . $row['PlantfamilyID'] . "</p></div>";
                                    ?>


                                        <div class=form-group col-md-6>
                                            <p>รหัสต้นไม้: <?php echo $row['plantlocationID']; ?></p>
                                            <input type='hidden' class='form-control' value='<?php echo $row['plantlocationID']; ?>' name='plantlocationID'>
                                        </div>
                                        <div class=form-group col-md-6>
                                            <p>วันที่: <?php echo $row['statusDate']; ?></p>
                                            <input type='hidden' class='form-control' value='<?php echo $row['statusDate']; ?>' name='statusDate'>
                                        </div>
                                </div>
                                <div class="from-row">
                                    <div class=form-group col-md-6>
                                        <label for=inputEmail4>รหัสพื้นที่</label>
                                        <input type='text' class='form-control' value='<?php echo $row['ZoneID']; ?>' name='ZoneID'>
                                    </div>

                                    <div class=form-group col-md-6>
                                        <label for=inputEmail4>รหัสพรรณไม้</label>
                                        <input type='text' class='form-control' value='<?php echo $row['PlandetailtID']; ?>' name='PlandetailtID'>
                                    </div>
                                </div>

                                <div class=form-group>
                                    <label for=PlantCommonname>ลองจิจูด</label>
                                    <input type='text' class='form-control' value='<?php echo $row['longtitudeY']; ?>' name='longtitudeY' placeholder=''>
                                </div>

                                <div class=form-group>
                                    <label for=PlantType>ละติจูด</label>
                                    <input type='text' class='form-control' value='<?php echo $row['latitudeX']; ?>' name='latitudeX' placeholder='ประเภท เช่น ไม้ยืนต้น'>
                                </div>

                                <div class=form-group>
                                    <label for=PlantTypeENG>สถานะ</label>
                                    <input type='text' class='form-control' value='<?php echo $row['statuss']; ?>' name='statuss' placeholder='ประเภท เช่น ไม้ยืนต้น'>
                                </div>

                            <?php
                                    }
                                    mysqli_close($conn);
                            ?>
                            <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2020</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <?php
        include('material_Logout_Modal.php');
        ?>

        <!-- script -->
        <?php
        include('material_script.php');
        ?>
    <?php
    } else {
        echo "<h1>Please login first .</h1>";
        echo "<a class='btn btn-primary' href='admin_singin.php'>หน้าหลัก</a>";
    }
    ?>
</body>

</html>