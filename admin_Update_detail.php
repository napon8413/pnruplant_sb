<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">

<?php
include('material_head.php');
?>


<body id="page-top">
    <?php
    if ($_SESSION["user_login"]) {
    ?>
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-tree"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">PNRU PLANT ADMIN </div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <?php
                include('material_Nav_Item_Dashboard.php');
                ?>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">
            </ul>
            <!-- End of Sidebar -->



            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->


                        <!-- Topbar Navbar -->
                        <?php
                        include('material_admin_topbar.php');
                        ?>

                    </nav>
                    <!-- End of Topbar -->



                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">เพิ่มข้อมูลพรรณไม้ใหม่</h1>
                        <div class="container">
                            <form action="admin_update_sql_detail.php" method="post">
                                <div class="form-row">

                                    <?php
                                    //รับ parameter มาเก็บในตัวแปร ID
                                    $ID = $_GET["ID"];
                                    //1. เชื่อมต่อ database: 
                                    include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

                                    // สิ่งที่ต้องการคือ 63201-10220-99-xxx เราไม่ต้องการ /001 ข้างหลัง
                                    // 2.1 substr เพื่อตัดรหัส 63201-10220-99-xxx ออก และเก็บในตัวแปร subID
                                    $subID = substr($ID, 18);
                                    // ใช้ trim ตัด /001 ที่เก็บในตัวแปร subID เราก็จะได้ 63201-10220-99-xxx เพื่อไป query ในตารางplantdetail
                                    $trimID = trim($ID, $subID);
                                    //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                    //2. query ข้อมูลจากตาราง plant: 
                                    $query = "SELECT * FROM `plantdetail` WHERE plantdetail.PlandetailtID = '" . $ID . "' " or die("Error:" . mysqli_error());
                                    $result = mysqli_query($conn, $query);

                                    // จะแก้โดยการ เอาตัวแปร id แสดงรหัสและตักคำไป query มาจากฐานข้อมูล

                                    while ($row = mysqli_fetch_array($result)) {
                                        // echo "<div><p>PlantID : " . $ID . "</p></div>";
                                        // echo "<div><p>PlantName: " . $row['PlantName'] . "</p></div>";
                                        // echo "<div><p>PlantScience : <i>" . $row['PlantCommonname'] . "</i> " . $row['PlantDiscover'] . "</p></div>";
                                        // echo "<div><p>PlantDiscover : " . $row['PlantDiscover'] . "</p></div>";
                                        // echo "<div><p>PlantCommonname : " . $row['PlantCommonname'] . "</p></div>";
                                        // echo "<div><p>PlantType : " . $row['PlantType'] . "</p></div>";
                                        // echo "<div><p>PlantDistrbution : " . $row['PlantDistrbution'] . "</p></div>";
                                        // echo "<div><p>PlantBenefit : " . $row['PlantBenefit'] . "</p></div>";
                                        // echo "<div><p>PlantBanefity : " . $row['PlantBanefity'] . "</p></div>";
                                        // // echo "<div><p>PlantIcon : ".$row['PlantIcon']."</p></div>";
                                        // echo "<div><p>PlantFlower : " . $row['PlantFlower'] . "</p></div>";
                                        // echo "<div><p>PlantRound : " . $row['PlantRound'] . "</p></div>";
                                        // echo "<div><p>PlantSeed : " . $row['PlantSeed'] . "</p></div>";
                                        // echo "<div><p>PlantStem : " . $row['PlantStem'] . "</p></div>";
                                        // echo "<div><p>PlantLeaf : " . $row['PlantLeaf'] . "</p></div>";
                                        // echo "<div><p>SeasonID : " . $row['SeasonID'] . "</p></div>";
                                        // echo "<div><p>PlantfamilyID :" . $row['PlantfamilyID'] . "</p></div>";
                                    ?>

                                        <!-- PlandetailtID -->
                                        <div class=form-group col-md-6>
                                            <label for=inputEmail4>รหัสพรรณไม้</label>
                                            <input type='text' class='form-control' name=PlandetailtID value='<?php echo $row['PlandetailtID']; ?>'>
                                        </div>
                                        <!-- PlantName -->
                                        <div class=form-group col-md-6>
                                            <label for=inputEmail4>ชื่อพรรณไม้</label>
                                            <input type='text' class='form-control' value='<?php echo $row['PlantName']; ?>' name='PlantName'>
                                        </div>
                                        <!-- PlantScience -->
                                        <div class=form-group col-md-6>
                                            <label for=inputEmail4>ชื่อวิทยาศาสตร์</label>
                                            <input type='text' class='form-control' value='<?php echo $row['PlantScience']; ?>' name='PlantScience'>
                                        </div>
                                        <!-- PlantDiscover -->
                                        <div class=form-group col-md-6>
                                            <label for=PlantDiscover>ชื่อผู้ค้นพบ</label>
                                            <input type='text' class='form-control' value='<?php echo $row['PlantDiscover']; ?>' name='PlantDiscover'>
                                        </div>
                                </div>
                                <!-- PlantCommonname -->
                                <div class=form-group>
                                    <label for=PlantCommonname>ชื่อสามัญ</label>
                                    <input type='text' class='form-control' value='<?php echo $row['PlantCommonname']; ?>' name='PlantCommonname' placeholder=''>
                                </div>
                                <!-- PlantType -->
                                <div class=form-group>
                                    <label for=PlantType>ประเภท</label>
                                    <input type='text' class='form-control' value='<?php echo $row['PlantType']; ?>' name='PlantType' placeholder='ประเภท เช่น ไม้ยืนต้น'>
                                </div>
                                <!-- PlantTypeENG -->
                                <div class=form-group>
                                    <label for=PlantTypeENG>PlantTypeENG</label>
                                    <input type='text' class='form-control' value='<?php echo $row['PlantTypeENG']; ?>' name='PlantTypeEng' placeholder='ประเภท เช่น ไม้ยืนต้น'>
                                </div>
                                <!-- PlantDistrbution -->
                                <div class=form-group>
                                    <label for=PlantDistrbution>สถานที่ค้นพบ</label>
                                    <textarea type='text' class='form-control' name='PlantDistrbution' rows=4><?php echo $row['PlantDistrbution']; ?></textarea>
                                </div>
                                <!-- PlantDistrbutionEng -->
                                <div class=form-group>
                                    <label for=PlantDistrbutionEng>PlantDistrbutionEng</label>
                                    <textarea type='text' class='form-control' name='PlantDistrbutionEng' rows=4><?php echo $row['PlantDistrbutionEng']; ?></textarea>
                                </div>
                                <!-- PlantBenefit -->
                                <div class=form-group>
                                    <label for=PlantBenefit>ประโยชน์</label>
                                    <textarea type='text' class='form-control' name='PlantBenefit' rows=4><?php echo $row['PlantBenefit']; ?></textarea>
                                </div>
                                <!-- PlantBenefitEng -->
                                <div class=form-group>
                                    <label for=PlantBenefitEng>PlantBenefitEng</label>
                                    <textarea type='text' class='form-control' name='PlantBenefitEng' rows=4><?php echo $row['PlantBenefitEng']; ?></textarea>
                                </div>
                                <!-- PlantBanefity -->
                                <div class=form-group>
                                    <label for=PlantBanefity>ประโยชน์อื่นๆ</label>
                                    <textarea type='text' class='form-control' name='PlantBanefity' rows=4><?php echo $row['PlantBanefity']; ?></textarea>
                                </div>
                                <!-- PlantBanefityEng -->
                                <div class=form-group>
                                    <label for=PlantBanefityEng>PlantBanefityEng</label>
                                    <textarea type='text' class='form-control' name='PlantBanefityEng' rows=4><?php echo $row['PlantBanefityEng']; ?></textarea>
                                </div>
                                <!-- PlantFlower -->
                                <div class=form-group>
                                    <label for=PlantFlower>รายละเอียดดอก</label>
                                    <textarea type='text' class='form-control' name='PlantFlower' rows=4><?php echo $row['PlantFlower']; ?></textarea>
                                </div>
                                <!-- PlantFlowerEng -->
                                <div class=form-group>
                                    <label for=PlantFlowerEng>PlantFlowerEng</label>
                                    <textarea type='text' class='form-control' name='PlantFlowerEng' rows=4><?php echo $row['PlantFlowerEng']; ?></textarea>
                                </div>
                                <!-- PlantRound -->
                                <div class=form-group>
                                    <label for=PlantRound>รายละเอียดผล</label>
                                    <textarea type='text' class='form-control' name='PlantRound' rows=4><?php echo $row['PlantRound']; ?></textarea>
                                </div>
                                <!-- PlantRoundEng -->
                                <div class=form-group>
                                    <label for=PlantRoundEng>PlantRoundEng</label>
                                    <textarea type='text' class='form-control' name='PlantRoundEng' rows=4><?php echo $row['PlantRoundEng']; ?></textarea>
                                </div>
                                <!-- PlantSeed -->
                                <div class=form-group>
                                    <label for=PlantSeed>รายละเอียดเมล็ด</label>
                                    <textarea type='text' class='form-control' name='PlantSeed' rows=4><?php echo $row['PlantSeed']; ?></textarea>
                                </div>
                                <!-- PlantSeedEng -->
                                <div class=form-group>
                                    <label for=PlantSeedEng>PlantSeedEng</label>
                                    <textarea type='text' class='form-control' name='PlantSeedEng' rows=4><?php echo $row['PlantSeedEng']; ?></textarea>
                                </div>
                                <!-- PlantStem -->
                                <div class=form-group>
                                    <label for=PlantStem>รายละเอียดลำต้น</label>
                                    <textarea type='text' class='form-control' name='PlantStem' rows=4><?php echo $row['PlantStem']; ?></textarea>
                                </div>
                                <!-- PlantStemEng -->
                                <div class=form-group>
                                    <label for=PlantStemEng>PlantStemEng</label>
                                    <textarea type='text' class='form-control' name='PlantStemEng' rows=4><?php echo $row['PlantStemEng']; ?></textarea>
                                </div>
                                <!-- PlantLeaf -->
                                <div class=form-group>
                                    <label for=PlantLeaf>รายละเอียดใบ</label>
                                    <textarea type='text' class='form-control' name='PlantLeaf' rows=4><?php echo $row['PlantLeaf']; ?></textarea>
                                </div>
                                <!-- PlantLeafEng -->
                                <div class=form-group>
                                    <label for=PlantLeafEng>PlantLeafEng</label>
                                    <textarea type='text' class='form-control' name='PlantLeafEng' rows=4><?php echo $row['PlantLeafEng']; ?></textarea>
                                </div>


                                <?php
                                    include('material_admin_loop_familyname.php');
                                ?>

                            <?php
                                    }
                                    mysqli_close($conn);
                            ?>
                            <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2020</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <?php
        include('material_Logout_Modal.php');
        ?>

        <!-- script -->
        <?php
        include('material_script.php');
        ?>

    <?php
    } else {
        echo "<h1>Please login first .</h1>";
        echo "<a class='btn btn-primary' href='admin_singin.php'>หน้าหลัก</a>";
    }
    ?>
</body>

</html>