<div class=form-row>
<div class="form-group col-md-4">
    <label for="inputSeasonID">ฤดู</label>
    <select name="inputSeasonID" class="form-control">
        <?php
        include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

        //2. query ข้อมูลจากตาราง tb_member: 
        $queryseason = "SELECT * FROM `season`" or die("Error:" . mysqli_error());
        //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
        $resultsseason = mysqli_query($conn, $queryseason);

        //4 . แสดงข้อมูลที่ query ออกมา โดยใช้ตารางในการจัดข้อมูล: 
        while ($rowseason = mysqli_fetch_assoc($resultsseason)) {
            echo "<option value='" . $rowseason['SeasonID '] . "'>" . $rowseason['SeasonName'] . "</option>";
        }
        mysqli_close($conn);
        ?>
    </select>
</div>

<div class="form-group col-md-4">
    <label for="inputPlantfamilyID">PlantfamilyID</label>
    <select name="PlantfamilyID" class="form-control">
        <?php
        include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

        //2. query ข้อมูลจากตาราง tb_member: 
        $queryfamily = "SELECT * FROM `plantfamily`" or die("Error:" . mysqli_error());
        //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
        $resultsfamily = mysqli_query($conn, $queryfamily);

        //4 . แสดงข้อมูลที่ query ออกมา โดยใช้ตารางในการจัดข้อมูล: 
        while ($rowfamily = mysqli_fetch_assoc($resultsfamily)) {
            echo "<option value='" . $rowfamily['PlantfamilyID'] . "'>" . $rowfamily['PlantfamilyName'] . " " . $rowfamily['PlantfamilyNameENG'] . "</option>";
        }
        
        ?>
    </select>
</div>
</div>