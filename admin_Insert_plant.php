<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">

<?php
include('material_head.php');
?>

<body id="page-top">
    <?php
    if ($_SESSION["user_login"]) {
    ?>
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-tree"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">PNRU PLANT ADMIN </div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <?php
                include('material_Nav_Item_Dashboard.php');
                ?>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">
            </ul>
            <!-- End of Sidebar -->



            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->


                        <!-- Topbar Navbar -->
                        <?php
                        include('material_admin_topbar.php');
                        ?>

                    </nav>
                    <!-- End of Topbar -->



                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">เพิ่มต้นไม้ใหม่</h1>
                        <div>
                            <form action="admin_insert_sql_plant.php" method="post">
                            <div class="form-group">
                                    <!-- PlandetailtID -->
                                    <label for="PlandetailtID">รหัสต้นไม้</label>
                                    <select name="PlandetailtID" class="form-control">
                                        <?php
                                        include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

                                        //2. query ข้อมูลจากตาราง tb_member: 
                                        $queryPlandetailtID = "SELECT PlandetailtID, PlantName FROM `plantdetail` ORDER BY `PlandetailtID` DESC" or die("Error:" . mysqli_error());
                                        //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                        $resultsPlandetailtID = mysqli_query($conn, $queryPlandetailtID);

                                        //4 . แสดงข้อมูลที่ query ออกมา โดยใช้ตารางในการจัดข้อมูล: 
                                        while ($rowPlandetailtID = mysqli_fetch_assoc($resultsPlandetailtID)) {
                                            echo "<option value='" . $rowPlandetailtID['PlandetailtID'] . "'>" . $rowPlandetailtID['PlandetailtID'].": ".$rowPlandetailtID['PlantName'] . "</option>";
                                        }
                                        mysqli_close($conn);
                                        ?>
                                    </select>
                                </div>
                                
                                
                                <div class="form-group">
                                    <!-- ZoneID -->
                                    <label for="ZoneID">รหัสพื้นที่ ZoneID</label>
                                    <input type="text" class="form-control" name="ZoneID" placeholder="CB01">
                                </div>
                                <div class="form-group">
                                    <!-- longtitudeY -->
                                    <label for="longtitudeY">ลองจิจูดlongtitudeY</label>
                                    <input type="text" class="form-control" name="longtitudeY" placeholder="">
                                </div>
                                <div class="form-group">
                                    <!-- latitudeX -->
                                    <label for="latitudeX">ละติจูด latitudeX</label>
                                    <input type="text" class="form-control" name="latitudeX" placeholder="">
                                </div>
                                <div class="form-group">
                                    <!-- statuss -->
                                    <label for="statuss">สถานะ status</label>
                                    <input type="text" class="form-control" name="statuss" placeholder="">
                                </div>
                                <input name="" id="" class="btn btn-primary" type="submit" value="เพิ่มต้นไม้ใหม่">
                            </form>
                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2020</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <?php
        include('material_Logout_Modal.php');
        ?>

        <!-- script -->
        <?php
        include('material_script.php');
        ?>
    <?php
    } else {
        echo "<h1>Please login first .</h1>";
        echo "<a class='btn btn-primary' href='admin_singin.php'>หน้าหลัก</a>";
    }
    ?>
</body>

</html>