<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">

<?php
include('material_head.php');
?>

<body id="page-top">
<?php
if($_SESSION["user_login"]) {
?>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-tree"></i>
                </div>
                <div class="sidebar-brand-text mx-3">PNRU PLANT ADMIN </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <?php
            include('material_Nav_Item_Dashboard.php');
            ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
        </ul>
        <!-- End of Sidebar -->



        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->


                    <!-- Topbar Navbar -->
                    <?php
                    include('material_admin_topbar.php');
                    ?>

                </nav>
                <!-- End of Topbar -->



                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- main -->
                    <div class="container">
                        <h1>แสดงข้อมูล</h1>
                        <?php
                        //รับ parameter มาเก็บในตัวแปร ID
                        $ID = $_GET["ID"];
                        //1. เชื่อมต่อ database: 
                        include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

                        //2. query ข้อมูลจากตาราง plant: 
                        $query = "SELECT * FROM `plantdetail` WHERE plantdetail.PlandetailtID = '" . $ID . "' ";
                        // echo $query;
                        $result = mysqli_query($conn, $query);
                        // จะแก้โดยการ เอาตัวแปร id แสดงรหัสและตักคำไป query มาจากฐานข้อมูล
                        while ($row = mysqli_fetch_array($result)) {
                            echo "<div><p>รหัสพรรณไม้ : " . $ID . "</p></div>";
                            echo "<div><p>ชื่อพรรณไม้: " . $row['PlantName'] . "</p></div>";
                            echo "<div><p>ชื่อวิทยาศาสตร์ : <i>" . $row['PlantCommonname'] . "</i> " . $row['PlantDiscover'] . "</p></div>";
                            echo "<div><p>ชื่อผู้ค้นพบ : " . $row['PlantDiscover'] . "</p></div>";
                            echo "<div><p>ชื่อสามัญ : " . $row['PlantCommonname'] . "</p></div>";
                            echo "<div><p>ประเภท : " . $row['PlantType'] . "</p></div>";
                            echo "<div><p>PlantType : " . $row['PlantTypeENG'] . "</p></div>";
                            echo "<div><p>สถานที่ค้นพบ : " . $row['PlantDistrbution'] . "</p></div>";
                            echo "<div><p>PlantDistrbution : " . $row['PlantDistrbutionEng'] . "</p></div>";
                            echo "<div><p>ประโยชน์ : " . $row['PlantBenefit'] . "</p></div>";
                            echo "<div><p>PlantBenefit : " . $row['PlantBenefitEng'] . "</p></div>";
                            echo "<div><p>ประโยชน์อื่นๆ : " . $row['PlantBanefity'] . "</p></div>";
                            echo "<div><p>PlantBanefity : " . $row['PlantBanefityEng'] . "</p></div>";
                            echo "<div><p>รายละเอียดดอก : " . $row['PlantFlower'] . "</p></div>";
                            echo "<div><p>PlantFlower : " . $row['PlantFlowerEng'] . "</p></div>";


                            echo "<div><p>รายละเอียดผล : " . $row['PlantRound'] . "</p></div>";
                            echo "<div><p>PlantRound : " . $row['PlantRoundEng'] . "</p></div>";


                            echo "<div><p>รายละเอียดเมล็ด : " . $row['PlantSeed'] . "</p></div>";
                            echo "<div><p>PlantSeed : " . $row['PlantSeedEng'] . "</p></div>";


                            echo "<div><p>รายละเอียดลำต้น : " . $row['PlantStem'] . "</p></div>";
                            echo "<div><p>PlantStem : " . $row['PlantStemEng'] . "</p></div>";


                            echo "<div><p>รายละเอียดใบ : " . $row['PlantLeaf'] . "</p></div>";
                            echo "<div><p>PlantLeaf : " . $row['PlantLeafEng'] . "</p></div>";



                            echo "<div><p>ฤดู : " . $row['SeasonID'] . "</p></div>";
                            echo "<div><p>family :" . $row['PlantfamilyID'] . "</p></div>";
                        }
                        mysqli_close($conn);
                        ?>
                        <a href="admin_detail_table.php" class="btn btn-primary">กลับหน้าเดิม</a>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <?php
include('material_Logout_Modal.php');
?>

    <!-- script -->
<?php
include('material_script.php');
?>
    <?php
}else {
    echo "<h1>Please login first .</h1>";
echo "<a class='btn btn-primary' href='admin_singin.php'>หน้าหลัก</a>";
}
?>
</body>

</html>