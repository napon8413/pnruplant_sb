<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">

<?php
include('material_head.php');
?>

<body id="page-top">
    <?php
    if ($_SESSION["user_login"]) {
    ?>
        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-tree"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">PNRU PLANT ADMIN </div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <?php
                include('material_Nav_Item_Dashboard.php');
                ?>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">
            </ul>
            <!-- End of Sidebar -->



            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Search -->

                        <!-- Topbar Navbar -->
                        <?php
                        include('material_admin_topbar.php');
                        ?>

                    </nav>
                    <!-- End of Topbar -->



                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- main -->
                        <div class="container">
                            <h1>แสดงข้อมูล</h1>
                            <?php
                            //รับ parameter มาเก็บในตัวแปร ID
                            $ID = $_GET["ID"];
                            //1. เชื่อมต่อ database: 
                            include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

                            //2. query ข้อมูลจากตาราง plant: 
                            $query = "SELECT * FROM area WHERE area.plantlocationID = '" . $ID . "' ";
                            $result = mysqli_query($conn, $query);
                            // จะแก้โดยการ เอาตัวแปร id แสดงรหัสและตักคำไป query มาจากฐานข้อมูล
                            while ($row = mysqli_fetch_array($result)) {

                                echo "<div><p>รหัสต้นไม้: " . $row['plantlocationID'] . "</p></div>";
                                echo "<div><p>รหัสพื้นที่: " . $row['ZoneID'] . "</p></div>";
                                echo "<div><p>รหัสพรรณไม้ : " . $row['PlandetailtID'] . "</p></div>";
                                echo "<div><p>ลองจิจูด : " . $row['longtitudeY'] . "</p></div>";
                                echo "<div><p>ละติจูด  : " . $row['latitudeX'] . "</p></div>";
                                echo "<div><p>สถานะ : " . $row['statuss'] . "</p></div>";
                                echo "<div><p>วันที่ : " . $row['statusDate'] . "</p></div>";
                                // echo "<div><p>qrcode : " . $row['qrcode'] . "</p></div>";

                            }
                            mysqli_close($conn);
                            ?>
                            <a href="admin_plant_table.php" class="btn btn-primary">กลับหน้าเดิม</a>
                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2020</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <?php
        include('material_Logout_Modal.php');
        ?>

        <!-- script -->
        <?php
        include('material_script.php');
        ?>
    <?php
    } else {
        echo "<h1>Please login first .</h1>";
        echo "<a class='btn btn-primary' href='admin_singin.php'>หน้าหลัก</a>";
    }
    ?>
</body>

</html>