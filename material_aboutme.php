<div class="jumbotron jumbotron-fluid p-5 text-center text-md-left">
        <div class="row" id="footer">
            <div class="col-md-4">
                <a class="navbar-brand" href="index.php">
                    <img src="image/pnru_logo.png" width="35" height="35" class="d-inline-block align-top" alt="">
                    PNRU
                    <img src="image/rspg_logo.jpg" width="35" height="35" class="d-inline-block align-top" alt="">
                    PNRU
                </a>
                <p>
                    <i class="fa fa-phone-square"></i>  02-544-8456 <br>
                    <!-- <i class="fa fa-envelope"></i> email@example.com <br> -->
                    <i class="fa fa-address-card"></i>  เลขที่ 9 แจ้งวัฒนะ แขวงอนุสาวรีย์ เขตบางเขน จังหวัดกรุงเทพฯ 10220
                </p>
                <a href="https://www.facebook.com/PhranakhonRajabhatUniversity" target="_blank">
                    <i class="fab fa-facebook-square fa-2x"></i>
                </a>
            </div>
            <!-- <div class="col-md-3">
                <h4>เมนู</h4>
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">หน้าหลัก <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">เกี่ยวกับเรา</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#input">เพิ่มข้อมูล</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#all">รายชื่อพรรณไม้ทั้งหมด</a>
                    </li>
                </ul>
            </div> -->
            <div class="col-md-5">
                <!-- GOOGLE MAP -->
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKb9-BF6y7fAD_s3s-or9knbdzInJdubw&callback=initMap&libraries=&v=weekly" defer></script>
                <script>
                    // Initialize and add the map
                    function initMap() {
                        // The location of Uluru
                        const uluru = {
                            lat: 13.878221465686897,
                            lng: 100.58984682648837
                        };
                        // The map, centered at Uluru
                        const map = new google.maps.Map(document.getElementById("map"), {
                            zoom: 15,
                            center: uluru,
                        });
                        // The marker, positioned at Uluru
                        const marker = new google.maps.Marker({
                            position: uluru,
                            map: map,
                        });
                    }
                </script>
                <h4>แผนที่</h4>
                <div id="map"></div>
            </div>
        </div>
    </div>