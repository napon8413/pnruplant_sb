<!DOCTYPE html>
<?php
session_start();
?>
<html lang="en">

<?php
include('material_head.php');
?>

<body id="page-top">
<?php
if($_SESSION["user_login"]) {
?>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-tree"></i>
                </div>
                <div class="sidebar-brand-text mx-3">PNRU PLANT ADMIN </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <?php
            include('material_Nav_Item_Dashboard.php');
            ?>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->


                    <!-- Topbar Navbar -->
                    <?php
                    include('material_admin_topbar.php');
                    ?>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- DataTales Example 2-->
                    <div class="card shadow mb-4" id="alldetail">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">พรรณไม้ทั้งหมด</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <?php

                                        //1. เชื่อมต่อ database: 
                                        include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี
                                        //2. query ข้อมูลจากตาราง tb_member: 
                                        $query3 = "SELECT PlandetailtID, PlantName FROM `plantdetail`" or die("Error:" . mysqli_error());
                                        //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
                                        $result3 = mysqli_query($conn, $query3);
                                        ?>
                                        <tr>
                                            <th>รหัสพรรณไม้</th>
                                            <th>ชื่อพรรณไม้</th>

                                            <th>แก้ไข</th>
                                            <th>ลบ</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>รหัสพรรณไม้</th>
                                            <th>ชื่อพรรณไม้</th>

                                            <th>แก้ไข</th>
                                            <th>ลบ</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        while ($row3 = mysqli_fetch_array($result3)) {
                                            echo "<tr>";
                                            echo "<th><a href='admin_detail.php?ID=$row3[0]'>" . $row3["PlandetailtID"] .  "</th> ";
                                            echo "<td>" . $row3["PlantName"] .  "</td> ";
                                            //เมนูดูข้อมูลอัพเดท
                                            echo "<td><a href='admin_Update_detail.php?ID=$row3[0]'>แก้ไขข้อมูล</a><br></td>  ";
                                            //ลบข้อมูล
                                            echo "<td><a href='admin_Deletedetail.php?ID=$row3[0]' onclick=\"return confirm('คุณต้องการลบพรรณไม้ " . $row3["PlandetailtID"] . " ใช่ไหม')\">ลบข้อมูล</a></td> ";
                                            echo "</tr>";
                                        }

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2020</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <?php
include('material_Logout_Modal.php');
?>

    <!-- script -->
<?php
include('material_script.php');
?>

    
    <?php
}else {
    echo "<h1>Please login first .</h1>";
echo "<a class='btn btn-primary' href='admin_singin.php'>หน้าหลัก</a>";
}
?>
</body>

</html>