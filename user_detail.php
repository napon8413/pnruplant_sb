<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">

    <title>รายละเอียดพรรณไม้ไม้</title>
   
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyBKb9-BF6y7fAD_s3s-or9knbdzInJdubw" type="text/javascript"></script>
</head>

<body>


    <!-- !Section Navbar -->
    <nav id="navbar" class="navbar navbar-expand-lg position-sticky navbar-dark bg-alpha">
        <div class="container">
            <a class="navbar-brand" href="index.php">
                <img src="image/pnru_logo.png" width="35" height="35" class="d-inline-block align-top" alt="">
                PNRUPLANT
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarKey" aria-controls="navbarKey" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarKey">
                <ul class="navbar-nav ml-auto text-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">หน้าหลัก <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">เกี่ยวกับเรา</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="#input">เพิ่มข้อมูล</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href="admin_singin.php">Admin login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <!-- main -->
    <div class="container">
        <h1>แสดงข้อมูล</h1>
        <?php
        //รับ parameter มาเก็บในตัวแปร ID
        $ID = $_GET["ID"];
        //1. เชื่อมต่อ database: 
        include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี

        //2. query ข้อมูลจากตาราง plant: 
        $query = "SELECT * FROM `plantdetail` WHERE plantdetail.PlandetailtID = '" . $ID . "' ";
        $query2 = "SELECT * FROM images WHERE images.plantlocationID= '" . $ID . "' ";
        // echo $query;
        $result = mysqli_query($conn, $query);
        $result2 = mysqli_query($conn, $query);
        // จะแก้โดยการ เอาตัวแปร id แสดงรหัสและตักคำไป query มาจากฐานข้อมูล
        while ($row = mysqli_fetch_array($result)) {
            echo "<div><p>รหัสพรรณไม้ : " . $ID . "</p></div>";
            echo "<div><p>ชื่อพรรณไม้: " . $row['PlantName'] . "</p></div>";
            echo "<div><p>ชื่อวิทยาศาสตร์ : <i>" . $row['PlantCommonname'] . "</i> " . $row['PlantDiscover'] . "</p></div>";
            echo "<div><p>ชื่อผู้ค้นพบ : " . $row['PlantDiscover'] . "</p></div>";
            echo "<div><p>ชื่อสามัญ : " . $row['PlantCommonname'] . "</p></div>";
            echo "<div><p>ประเภท : " . $row['PlantType'] . "</p></div>";
            echo "<div><p>PlantType : " . $row['PlantTypeENG'] . "</p></div>";
            echo "<div><p>สถานที่ค้นพบ : " . $row['PlantDistrbution'] . "</p></div>";
            echo "<div><p>PlantDistrbution : " . $row['PlantDistrbutionEng'] . "</p></div>";
            echo "<div><p>ประโยชน์ : " . $row['PlantBenefit'] . "</p></div>";
            echo "<div><p>PlantBenefit : " . $row['PlantBenefitEng'] . "</p></div>";
            echo "<div><p>ประโยชน์อื่นๆ : " . $row['PlantBanefity'] . "</p></div>";
            echo "<div><p>PlantBanefity : " . $row['PlantBanefityEng'] . "</p></div>";




            echo "<div><p>รายละเอียดดอก : " . $row['PlantFlower'] . "</p></div>";
            echo "<div><p>PlantFlower : " . $row['PlantFlowerEng'] . "</p></div>";


            echo "<div><p>รายละเอียดผล : " . $row['PlantRound'] . "</p></div>";
            echo "<div><p>PlantRound : " . $row['PlantRoundEng'] . "</p></div>";


            echo "<div><p>รายละเอียดเมล็ด : " . $row['PlantSeed'] . "</p></div>";
            echo "<div><p>PlantSeed : " . $row['PlantSeedEng'] . "</p></div>";


            echo "<div><p>รายละเอียดลำต้น : " . $row['PlantStem'] . "</p></div>";
            echo "<div><p>PlantStem : " . $row['PlantStemEng'] . "</p></div>";


            echo "<div><p>รายละเอียดใบ : " . $row['PlantLeaf'] . "</p></div>";
            echo "<div><p>PlantLeaf : " . $row['PlantLeafEng'] . "</p></div>";



            echo "<div><p>ฤดู : " . $row['SeasonID'] . "</p></div>";
            echo "<div><p>family :" . $row['PlantfamilyID'] . "</p></div>";
        }
        mysqli_close($conn);
        ?>
        <div id="map_plant"></div>

<script type="text/javascript">
        <?php
        //1. เชื่อมต่อ database: 
        include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้
        //2. query ข้อมูล: 
        $queryloplant = "SELECT area.plantlocationID,area.longtitudeY,area.latitudeX,plantdetail.PlantName FROM area RIGHT JOIN plantdetail ON area.PlandetailtID = plantdetail.PlandetailtID WHERE area.PlandetailtID = '" . $ID . "'" or die("Error:" . mysqli_error());
        //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
        $resultloplant = mysqli_query($conn, $queryloplant);
        //4 . แสดงข้อมูลที่ query ออกมา โดยใช้ตารางในการจัดข้อมูล: 
        $resultloplant = $conn->query($queryloplant);
        // เอาข้อมู,ใส่ arry item
        if ($resultloplant->num_rows > 0) {
            while ($rowloplant = mysqli_fetch_array($resultloplant, MYSQLI_NUM)) {
                $item[] = $rowloplant;
            }
            // แปลงเป็น json
            $json = json_encode($item);
        } else {
            echo "No Data Found.";
        }
        ?>
        // เอาตัวแปร php มาใส่ในตัวแปร javascrip
        var locations = <?php echo $json; ?>;
        // loop markers ไปที่ div ID = map_plant
        var maploplant = new google.maps.Map(document.getElementById('map_plant'), {
            zoom: 18,
            // บอกตำแหน่งเป็ฯ json คือ json arry ที่ 0 เอา value จาก key ที่ 2 และ 1
            center: new google.maps.LatLng(locations[0][2], locations[0][1]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;
        // loop ข้อมูลใน json ออกมา
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][2], locations[i][1]),
                map: maploplant
            });
            // สร้าง event click แล้วขึ้นข้อมูล
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][3]);
                    infowindow.open(maploplant, marker);
                }
            })(marker, i));
        }
</script>
        <a href="index.php" class="btn btn-primary">กลับหน้าเดิม</a>
    </div>


    <!--!contact-->
    <?php
    include('material_aboutme.php');
    ?>

    <footer class="footer">
        <span> COPYRIGHT © 2020
            <a href="#" target="_blank">Soymilk</a>
            ALL Right Reserved
        </span>
    </footer>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="assets/js/main.js"></script>
</body>

</html>